# ILS

## now

[注册](https://now.sh) 账号，安装 now 命令行。

```shell
npm install -g now
```

命令行登陆账号

```shell
now login
```

在 `Dockerfile` 所在目录进行部署。

```shell
now
```

修改别名，方便记住 URL 地址。

```
now alias intellij-idea-license-server-vivoqgthem.now.sh idea
```

保持服务一直运行。

```shell
now scale https://idea.now.sh 1
```

最终我们得到的破解服务器地址就是 `https://idea.now.sh` 。

## 激活

![activate](https://d33wubrfki0l68.cloudfront.net/b35efc52c3d8ebf74dabcd43cdf7fe736adf2558/68bec/posts/10120/activate.png "激活服务器地址")

点击 `Activate` 即可激活。

![success](https://d33wubrfki0l68.cloudfront.net/780102cfa51b75ca2f317ca8034482b99b1e6688/6e128/posts/10120/success.png "激活完成")

## 其他

详情请参考 [搭建 IDEA License 服务](https://www.ouyangsong.com/posts/10120/) 。
